<!DOCTYPE html>
<html lang="zh" class="mag-js"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
    <title>picture</title>

    <link rel="stylesheet" href="./bootstrap.min.css">
    <link rel="stylesheet" href="./mag.css">
    <link rel="stylesheet" href="./default.css">
    <link rel="stylesheet" href="./index.css">

    <style type="text/css">
        .controls-btns button {
            color: #333;
        }

        .mt50 {
            margin-top: 50px;
        }
    </style>

</head>
<body style="">
    <div class="container mt50">
        <main>
            <div class="row mag-eg-row">
                <div class="col col-md-6">
                    <div class="mag-eg-el-wrap img-thumbnail">
                        <div class="proportion">
                            <div mag-thumb="inner" class="mag-eg-el mag-host" mag-mode="inner" mag-theme="default" mag-position="mirror" mag-position-event="move" mag-toggle="true"><div class="mag-noflow mag-zoomed-container mag-zoomed-bg" mag-theme="default" mag-toggle="true" style="display: none;"><div class="mag-zoomed" style="transform: scale3d(1, 1, 1) translate3d(27.9817%, -49.6942%, 0px); width: 100%; height: 100%; position: absolute; top: 0px; left: 0px;"><img src="./1000x600.jpg"></div></div><div class="mag-thumb"><img src="./500x300.jpg"></div><div class="mag-zone" style="touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div></div>
                            
                            <div class="filler"></div>
                        </div>
                    </div>
                </div>
                <div class="col col-md-6">
                    <div class="mag-eg-doc">
                        <h3>Inner</h3>
                        <pre><code class="lang-html"><!--
                        -->&lt;div mag-thumb="inner"&gt;
                        &lt;img src="img/alley/500x300.jpg" /&gt;
                        &lt;/div&gt;
                        &lt;div mag-zoom="inner"&gt;
                        &lt;img src="img/alley/1000x600.jpg" /&gt;
                        &lt;/div&gt;<!--
                                  --></code></pre>
                                                <pre><code class="lang-js"><!--
                        -->$host = $('[mag-thumb="inner"]');
                        $host.mag();<!--
                                  --></code></pre>
                    </div>
                </div>
            </div>

            <div class="row mag-eg-row">
                <div class="col col-md-6">
                    <div class="mag-eg-el-wrap img-thumbnail">
                        <div class="proportion">
                            <div mag-thumb="outer" class="mag-eg-el mag-host" mag-mode="outer" mag-theme="default" mag-position="mirror" mag-position-event="move" mag-toggle="true"><div class="mag-lens" style="display: none; transform: scale3d(0.5, 0.8, 1) translate3d(50%, -12.5%, 0px); width: 100%; height: 100%; position: absolute; top: 0px; left: 0px;"></div><div class="mag-noflow" mag-theme="default"></div><div class="mag-thumb"><img src="./500x334.jpg"></div><div class="mag-zone" style="touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div></div>
                            <div class="filler"></div>
                        </div>
                    </div>
                    <div class="mag-eg-el-wrap img-thumbnail" style="height:auto;width:auto">
                        <div style="height: 300px; width: 300px">
                            <div mag-zoom="outer" class="mag-eg-el mag-zoomed-container mag-zoomed-bg" style="float: right; position: relative; overflow: hidden; display: none;" mag-theme="default" mag-toggle="true"><div class="mag-zoomed" style="transform: scale3d(2, 1.25, 1) translate3d(-42.2936%, 26.4526%, 0px); width: 100%; height: 100%; position: absolute; top: 0px; left: 0px;"><img src="./full.jpg"></div></div>
                        </div>
                    </div>
                </div>
                <div class="col col-md-6">
                    <div class="mag-eg-doc">
                        <h3>Outer</h3>
                        <pre><code class="lang-html"><!--
                        -->&lt;div mag-thumb="outer"&gt;
                        &lt;img src="img/alley/500x300.jpg" /&gt;
                        &lt;/div&gt;
                        &lt;div style="width: 300px; height: 300px;"&gt;
                        &lt;div mag-zoom="outer"&gt;
                        &lt;img src="img/alley/1000x600.jpg" /&gt;
                        &lt;/div&gt;
                        &lt;/div&gt;<!--
                                  --></code></pre>
                                                <pre><code class="lang-js"><!--
                        -->$host = $('[mag-thumb="outer"]');
                        $host.mag({
                        mode: 'outer',
                        ratio: 1 / 1.6
                        });
                    </div>
                </div>
            </div>
            
        </main>
    </div>

    <script src="./jquery.min.js"></script>
    <script src="./jquery.bridget.js"></script>
    <script src="./jquery.mousewheel.min.js"></script>
    <script src="./jquery.event.drag.js"></script>
    <script src="./screenfull.js"></script>
    <script src="./hammer.min.js"></script>
    <script src="./PreventGhostClick.js"></script>
    <script src="./mag-analytics.js"></script>
    <script src="./mag.js"></script>
    <script src="./mag-jquery.js"></script>
    <script src="./mag-control.js"></script>
    <script src="./index.js"></script>

 

</body></html>